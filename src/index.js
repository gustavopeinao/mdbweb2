import * as React from "react";
import * as ReactDOM from "react-dom";

import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "./index.css";

// import App from "./App";
import Container from './components/container/Container'
import registerServiceWorker from './registerServiceWorker';
import {HashRouter} from 'react-router-dom'

ReactDOM.render( 
    <HashRouter>
        <Container />
    </HashRouter>,
    document.getElementById('root')
);

registerServiceWorker();
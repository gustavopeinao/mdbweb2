import * as React from 'react';
import { MDBView, MDBMask, MDBCol, MDBCard,  } from 'mdbreact';
import "./Sedes.css";
import tutorias from "./tutorias.jpg";
import iconox from './iconox.ico';

function TutoriasPage () {
    function componentDidMount() {
        window.scrollTo(0, 0)
      } 
   
  return (
        <div>

            <div className="container-fluid" style={{padding:0}}>
                <div className="col-md-12" style={{padding:0}}>
                    <MDBCard className="card-image" style={{ backgroundImage: 'url(' + tutorias + ')'}}>
                      <div className="rgba-black-strong" style={{paddingTop:70, paddingBottom:20}}>

                          <div className="row ">
                                <div className="col-md-12 text-center">
                                    <img
                                            src={iconox}
                                            width="auto" 
                                            height="35"
                                            alt="logo"
                                            className="App-logo"        
                                    />
                                    <h3 className="text-white text-center font-weight" style={{paddingTop:5}}>
                                    Sedes UNJu
                                    </h3>
                                </div>
                          </div>
                          
                      </div>
                    </MDBCard>
                </div>
            </div>


            <div className="container">
                                    
                    <div className=" card col-md-12 " style={{paddingTop:60, paddingBottom:60, paddingLeft:15, paddingRight:15}}>

                            <p className="font-weight-light text-justify" style={{paddingBottom:30}}>Las Expansiones Académicas reflejan la regionalización universitaria y la  descentralización o desconcentración de servicios que brinda la UNJu. Constituye además uno de los mayores logros en la historia universitaria de nuestra provincia.</p>
                        
                            <iframe className="col-md-12 col-sm.12" style={{padding:0}} src="https://www.google.com/maps/d/u/0/embed?mid=1JZUOxR1-Oh8Hj4d5wnKwIii5ZM6j3hqM" width="1200" height="580"/>
                                        
                        
                        
                    </div>
                 
            </div>
        </div>
  );
}

export default TutoriasPage;
import * as React from 'react';
import { MDBView, MDBMask, MDBCol, MDBCard,  } from 'mdbreact';
//import "./Header.css";
import tutorias from "./tutorias.jpg";
import cap1 from "./cap1.jpg";
import cap2 from "./cap2.jpg";
import cap3 from "./cap3.jpg";
import iconox from './iconox.ico';

function CapacitacionPage () {

 function componentDidMount() {
        window.scrollTo(0, 0)
      } 
   
  return (
        <div>

            <div className="container-fluid" style={{padding:0}}>
                <div className="col-md-12" style={{padding:0}}>
                    <MDBCard className="card-image" style={{ backgroundImage: 'url(' + tutorias + ')'}}>
                      <div className="rgba-black-strong" style={{paddingTop:70, paddingBottom:20}}>

                          <div className="row ">
                                <div className="col-md-12 text-center">
                                    <img
                                            src={iconox}
                                            width="auto" 
                                            height="35"
                                            alt="logo"
                                            className="App-logo"        
                                    />
                                    <h3 className="text-white text-center font-weight" style={{paddingTop:5}}>
                                    Capacitación
                                    </h3>
                                </div>
                          </div>
                          
                      </div>
                    </MDBCard>
                </div>
            </div>


            <div className="container">
                                    
            <div className=" card col-md-12 " style={{paddingTop:60, paddingBottom:60, paddingLeft:15, paddingRight:15}}>
                                               
                        <p className="font-weight-light text-justify" style={{paddingBottom:50}}>La Coordinación de Extensión junto a Docentes e Investigadores de la UNJu desarrollan talleres de capacitación en “Tutorías Académicas” destinados a Graduados y profesionales universitarios con el propósito de desarrollar de manera conjunta diferentes dispositivos para el acompañamiento de los alumnos del último año del nivel secundario que deseen continuar con sus estudios universitarios.</p>
                        
                        {/* slider */}

                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100" src={cap1} alt="First slide"/>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src={cap2} alt="Second slide"/>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src={cap3} alt="Third slide"/>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>

                        {/* fin slider */}
                        
                    </div>
                 
            </div>
        </div>
  );
}

export default CapacitacionPage;
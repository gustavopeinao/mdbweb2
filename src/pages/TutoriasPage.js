import * as React from 'react';
import { MDBView, MDBMask, MDBCol, MDBCard,  } from 'mdbreact';
//import "./Header.css";
import tutorias from "./tutorias.jpg";
import iconox from './iconox.ico';

function TutoriasPage () {
    function componentDidMount() {
        window.scrollTo(0, 0)
      } 
   
  return (
        <div>

            <div className="container-fluid" style={{padding:0}}>
                <div className="col-md-12" style={{padding:0}}>
                    <MDBCard className="card-image" style={{ backgroundImage: 'url(' + tutorias + ')'}}>
                      <div className="rgba-black-strong" style={{paddingTop:70, paddingBottom:20}}>

                          <div className="row ">
                                <div className="col-md-12 text-center">
                                    <img
                                            src={iconox}
                                            width="auto" 
                                            height="35"
                                            alt="logo"
                                            className="App-logo"        
                                    />
                                    <h3 className="text-white text-center font-weight" style={{paddingTop:5}}>
                                    Tutorias Académicas
                                    </h3>
                                </div>
                          </div>
                          
                      </div>
                    </MDBCard>
                </div>
            </div>


            <div className="container">
                                    
            <div className=" card col-md-12 " style={{paddingTop:60, paddingBottom:60, paddingLeft:15, paddingRight:15}}>
                                               
                        <p className="font-weight-light text-justify">La Coordinación de Extensión de la Universidad Nacional de Jujuy brinda talleres informativos y de acompañamiento pedagógico a estudiantes de escuelas secundarias ubicadas geográficamente en las localidades en las que funcionan las sedes universitarias de la UNJu. (Humahuaca, Tilcara, San Salvador de Jujuy, San Pedro).</p>
                        <h3 className="font-weight-light text-center text-justify" style={{paddingBottom:30}}>Cronograma de Talleres programados de Tutorias</h3>
                        <table style={{marginLeft: "30px", marginBottom:200}}>
                            <tbody>
                            <tr>
                            <th>Lugar y Horario</th>
                            <th>Fecha</th>
                            
                            </tr>
                            <tr>
                            <td>Sede Tilcara - Esc. Normal - Casanova de 9:30 a 13:30hs</td>
                            <td>Viernes 03 de Mayo</td>
                            
                            </tr>
                            <tr>
                            
                            </tr>
                            <tr>
                            
                            </tr>
                            </tbody>
                        </table>
                        
                    </div>
                 
            </div>
        </div>
  );
}

export default TutoriasPage;
import * as React from 'react';
import { Link } from "react-router-dom";
import { BrowserRouter as Router} from 'react-router-dom';

import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBIcon } from "mdbreact";

import logoNav from './logoNav.png';
 

class Header extends React.Component{

    state = {
        isOpen: false
      };
      
      toggleCollapse = () => {
        this.setState({ isOpen: !this.state.isOpen });
      }  
    
   
    render() {
        return (
            
            <MDBNavbar color="default-color" dark expand="md" scrolling fixed="top">
                
                <MDBNavbarBrand style={{paddingLeft:30}}>
                    <img 
                        src={logoNav} 
                        width="30"  
                        alt=""
                    />  
                </MDBNavbarBrand>

                <MDBNavbarBrand>
                    <strong ><a className="white-text" href="">Nexos</a></strong>
                </MDBNavbarBrand>

                <MDBNavbarToggler onClick={this.toggleCollapse} />

                <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
                    <MDBNavbarNav left style={{paddingLeft:50}}>

                        <MDBNavItem>
                            <MDBNavLink to="/tutorias" className="font-weight-normal nav-link" style={{color:"white", fontSize:"18px" }}>Tutorías</MDBNavLink>
                        </MDBNavItem>

                        <MDBNavItem>
                            <MDBNavLink to="/capacitacion" className="font-weight-normal nav-link" style={{color:"white", fontSize:"18px" }}>Capacitación</MDBNavLink>
                        </MDBNavItem>

                        <MDBNavItem>
                            <MDBNavLink to="/sedes" className="font-weight-normal nav-link" style={{color:"white", fontSize:"18px"}}>Sedes</MDBNavLink>
                        </MDBNavItem>

                        <MDBNavItem>
                            <MDBNavLink to="/explorar" className="font-weight-normal nav-link" style={{color:"white", fontSize:"18px"}}>Explorar (me)</MDBNavLink>
                        </MDBNavItem>

                    </MDBNavbarNav>
                    
                    <MDBNavbarNav right style={{paddingRight:20}}>
                        <MDBNavItem>
                        <a style={{color:"white"}} href="https://www.facebook.com/UNJuTV/" target="_blank">
                        <MDBIcon fab icon="facebook" />
                        </a>
                        </MDBNavItem>
                    </MDBNavbarNav>
                </MDBCollapse>
                
                    

                
            </MDBNavbar>

        )
    }
}

export default Header;
import * as React from 'react';
import { MDBInput, MDBCard  } from 'mdbreact';
//import "./Header.css";
import tutorias from "./tutorias.jpg";

import iconox from './iconox.ico';
import guia from './guia.png'
//import './Explorar.css'
import Cuestionario from './Cuestionario'

class Explorar extends React.Component {

    

    render() { 
        return (
                <div>

                    <div className="container-fluid" style={{padding:0}}>
                        <div className="col-md-12" style={{padding:0}}>
                            <MDBCard className="card-image" style={{ backgroundImage: 'url(' + tutorias + ')'}}>
                            <div className="rgba-black-strong" style={{paddingTop:70, paddingBottom:20}}>

                                <div className="row ">
                                        <div className="col-md-12 text-center">
                                            <img
                                                    src={iconox}
                                                    width="auto" 
                                                    height="35"
                                                    alt="logo"
                                                    className="App-logo"        
                                            />
                                            <h3 className="text-white text-center font-weight" style={{paddingTop:5}}>
                                            Explorar (me)
                                            </h3>
                                        </div>
                                </div>
                                
                            </div>
                            </MDBCard>
                        </div>
                    </div>


                    <div className="container">
                                            
                        <div className="card" style={{paddingTop:60, paddingBottom:60, paddingLeft:15, paddingRight:15}}>
                            <div className="col-md-12">                            
                                    <p className="font-weight-light text-justify" style={{paddingBottom:0}}>¡Hola! <b>Explorar (me)</b> no es un test vocacional, es sólo una ayuda para entender mejor sus intereses y preferencias y tomar la mejor decisión en base a ello.
                                        Una vez que respondan las preguntas les resultará más fácil navegar en la página e investigar acerca del campo de interés o la carrera que llame su atención. 
                                        Si la carrera que capto su interés está entre las de la UNJu podrás ingresar al link con toda la información, pero si no, te dejamos a disposición el link de la Secretaría de Políticas Universitarias con la información de las carreras que se dictan en instituciones de nuestro país.
                                    </p>
                            </div>

                            <hr></hr>
                            <div className="col-md-12">
                                <h3>Cuestionario campos de interés profesional.</h3>
                                <p className="font-weight-light"><b>-</b> Lea atentamenta cada pregunta.<br></br><b>-</b> Marca.<br></br><b>-</b> Lee y responde todas las preguntas con “me interesa” o “no me interesa”. Es importante que no omitas ninguna pregunta.</p>
                            </div>
                            
                            
                            <Cuestionario />

                            
                            <div className="col-md-12">
                                    <p className="text-center font-italic">Si queres saber acerca de mas carreras hace Click en la Imagen</p>
                                    <a href="http://guiadecarreras.siu.edu.ar/" target="_blank">
                                        <img
                                                    src={guia}
                                                    alt=""
                                                    width="500"
                                                    height="auto"
                                                    className="logoe rounded mx-auto d-block img-fluid"
                                                />
                                    </a>
                            </div>
             
                        </div>
                    </div>
                </div>
        );
    }
}

export default Explorar;
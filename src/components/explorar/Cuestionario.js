import * as React from 'react';
import { Link, BrowserRouter as Router } from "react-router-dom";
import chicos from './chicos.png'

class Cuestionario extends React.Component {
    constructor() {
        super();
        this.state = {

          switchHuma: false,
          switchEco: false,
          switchSalud: false,
          switchSegu: false,
          switchIng: false,
          switchAgra: false,
          switchArt: false,
          

          contHuma: 0,
          contEco: 0,
          contSalud: 0,
          contSegu: 0,
          contIng: 0,
          contAgra: 0,
          contArt: 0,
        }
        
        this.handleSwitchHuma = this.handleSwitchHuma.bind(this);
        this.handleSwitchEco = this.handleSwitchEco.bind(this);
        this.handleSwitchSalud = this.handleSwitchSalud.bind(this);
        this.handleSwitchSegu = this.handleSwitchSegu.bind(this);
        this.handleSwitchIng = this.handleSwitchIng.bind(this);
        this.handleSwitchAgra = this.handleSwitchAgra.bind(this);
        this.handleSwitchArt = this.handleSwitchArt.bind(this);

        // this.handleEval = this.handleEval.bind(this);
        this.handleNewTest = this.handleNewTest.bind(this);
        
      }

      handleSwitchHuma(e) {
        this.setState({ switchHuma: e.target.checked });
        if (this.state.switchHuma == false) {
            this.setState({
                contHuma: this.state.contHuma + 1
            })
        }
        else{
            this.setState({
                contHuma: this.state.contHuma + 1
            })
        }       
    }

    handleSwitchEco(e) {
        this.setState({ switchEco: e.target.checked });
        if (this.state.switchEco == false) {
            this.setState({
                contEco: this.state.contEco + 1
            })
        } 
        else{
            this.setState({
                contEco: this.state.contEco + 1
            })
        }      
    }

    handleSwitchSalud(e) {
        this.setState({ switchSalud: e.target.checked });
        if (this.state.switchSalud == false) {
            this.setState({
                contSalud: this.state.contSalud + 1
            })
        } 
        else{
            this.setState({
                contSalud: this.state.contSalud + 1
            })
        }      
    }

    handleSwitchSegu(e) {
        this.setState({ switchSegu: e.target.checked });
        if (this.state.switchSegu == false) {
            this.setState({
                contSegu: this.state.contSegu + 1
            })
        }
        else{
            this.setState({
                contSegu: this.state.contSegu + 1
            })
        }       
    }

    handleSwitchIng(e) {
        this.setState({ switchIng: e.target.checked });
        if (this.state.switchIng == false) {
            this.setState({
                contIng: this.state.contIng + 1
            })
        } 
        else{
            this.setState({
                contIng: this.state.contIng + 1
            })
        }      
    }

    handleSwitchAgra(e) {
        this.setState({ switchAgra: e.target.checked });
        if (this.state.switchAgra == false) {
            this.setState({
                contAgra: this.state.contAgra + 1
            })
        }  
        else{
            this.setState({
                contAgra: this.state.contAgra + 1
            })
        }     
    }

    handleSwitchArt(e) {
        this.setState({ switchArt: e.target.checked });
        if (this.state.switchArt == false) {
            this.setState({
                contArt: this.state.contArt + 1
            })
        } 
        else{
            this.setState({
                contArt: this.state.contArt + 1
            })
        }      
    }

    // handleEval(){
    //     this.setState({
    //         contHuma: this.state.contHuma + 1,
    //         contEco: this.state.contEco + 1,
    //         contSalud: this.state.contSalud + 1,
    //         contSegu: this.state.contSegu + 1,
    //         contIng: this.state.contIng + 1,
    //         contAgra: this.state.contAgra + 1,
    //         contArt: this.state.contArt + 1,

    //     })
    // }
  
    handleNewTest(){
        // return location.reload();
        window.location.reload();
        // this.setState({

           
            

        //     contHuma: 0,
        //     switchHuma:false,

        //     contEco: 0,
        //     switchEco:false,

        //     contSalud: 0,
        //     switchSalud:false,

        //     contSegu: 0,
        //     switchSegu:false,

        //     contIng: 0,
        //     switchIng:false,

        //     contAgra: 0,
        //     switchAgra:false,

        //     contArt: 0,
        //     switchArt:false,


        // })
    }

    

    render() { 
        // const switch1 = "hola";
        return (
                <div>
                    
                    <div>
                    
                        {/* pregunta1 */}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" onClick={this.handleSwitchEco} class="custom-control-input" id="customCheck1"/>
                            <label class="custom-control-label" for="customCheck1">1- Asumir el rol de coordinador en un grupo de trabajo.</label>
                        </div>
                        {/* pregunta2 */}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox"  onClick={this.handleSwitchHuma} class="custom-control-input" id="customCheck2"/>
                            <label class="custom-control-label" for="customCheck2">2- Realizar excursiones y ser guía de un grupo de personas.</label>
                        </div>
                        {/* pregunta3*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" onClick={this.handleSwitchSalud} class="custom-control-input" id="customCheck3"/>
                            <label class="custom-control-label" for="customCheck3">3- Cuidar a pequeños y grandes animales.</label>
                        </div>
                        {/* pregunta4*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" onClick={this.handleSwitchIng} class="custom-control-input" id="customCheck4"/>
                            <label class="custom-control-label" for="customCheck4">4- Realizar experimentos para analizar y estudiar los fenómenos químicos de las sustancias.</label>
                        </div>
                        {/* pregunta5*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSegu" onClick={this.handleSwitchSegu} class="custom-control-input" id="customCheck5"/>
                            <label class="custom-control-label" for="customCheck5">5- Gestionar empresas de seguridad privada.</label>
                        </div>
                        {/* pregunta6*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" checked={this.state.defaultChecked} onClick={this.handleSwitchHuma} class="custom-control-input" id="customCheck6"/>
                            <label class="custom-control-label" for="customCheck6">6- Aprender sobre el pasado de la humanidad.</label>
                        </div>
                        {/* pregunta7*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchArt" onClick={this.handleSwitchArt} class="custom-control-input" id="customCheck7"/>
                            <label class="custom-control-label" for="customCheck7">7- Organizar fiestas o eventos especiales.</label>
                        </div>
                        {/* pregunta8*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchAgra" onClick={this.handleSwitchAgra} class="custom-control-input" id="customCheck8"/>
                            <label class="custom-control-label" for="customCheck8">8- Desarrollar tus actividades en viveros y plantaciones forestales.</label>
                        </div>
                        {/* pregunta9*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchEco" onClick={this.handleSwitchEco} class="custom-control-input" id="customCheck9"/>
                            <label class="custom-control-label" for="customCheck9">9- Trabajar en una oficina organizando las fichas de los archivos.</label>
                        </div>
                        {/* pregunta10*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchHuma" onClick={this.handleSwitchHuma} class="custom-control-input" id="customCheck10"/>
                            <label class="custom-control-label" for="customCheck10">10- Diseñar y desarrollar proyectos de acción comunitaria.</label>
                        </div>
                        {/* pregunta11*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSegu" onClick={this.handleSwitchSegu} class="custom-control-input" id="customCheck11"/>
                            <label class="custom-control-label" for="customCheck11">11- Participar en equipos de seguridad pública o privada.</label>
                        </div>
                        {/* pregunta12*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchIng" onClick={this.handleSwitchIng} class="custom-control-input" id="customCheck12"/>
                            <label class="custom-control-label" for="customCheck12">12- Examinar, analizar y estudiar el funcionamiento de máquinas nuevas e inventos tecnológicos.</label>
                        </div>
                        {/* pregunta13*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSalud" onClick={this.handleSwitchSalud} class="custom-control-input" id="customCheck13"/>
                            <label class="custom-control-label" for="customCheck13">13- Atender a la salud bucal de las personas.</label>
                        </div>
                        {/* pregunta14*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSegu" onClick={this.handleSwitchSegu} class="custom-control-input" id="customCheck14"/>
                            <label class="custom-control-label" for="customCheck14">14- Realizar tareas de socorrismo a personas accidentadas.</label>
                        </div>
                        {/* pregunta15*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchEco" onClick={this.handleSwitchEco} class="custom-control-input" id="customCheck15"/>
                            <label class="custom-control-label" for="customCheck15">15- Supervisar las ventas en un salón comercial.</label>
                        </div>
                        {/* pregunta16*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchIng" onClick={this.handleSwitchIng} class="custom-control-input" id="customCheck16"/>
                            <label class="custom-control-label" for="customCheck16">16- Diseñar juegos interactivos en computadora.</label>
                        </div>
                        {/* pregunta17*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchAgra" onClick={this.handleSwitchAgra} class="custom-control-input" id="customCheck17"/>
                            <label class="custom-control-label" for="customCheck17">17- Ser parte de un equipo de trabajo orientado a la preservación de la flora y fauna en extinción.</label>
                        </div>
                        {/* pregunta18*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSalud" onClick={this.handleSwitchSalud} class="custom-control-input" id="customCheck18"/>
                            <label class="custom-control-label" for="customCheck18">18- Participar en una operación en un quirófano.</label>
                        </div>
                        {/* pregunta19*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSegu" onClick={this.handleSwitchSegu} class="custom-control-input" id="customCheck19"/>
                            <label class="custom-control-label" for="customCheck19">19- Ser parte de las fuerzas armadas.</label>
                        </div>
                        {/* pregunta20*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchArt" onClick={this.handleSwitchArt} class="custom-control-input" id="customCheck20"/>
                            <label class="custom-control-label" for="customCheck20">20- Diseñar y producir objetos novedosos relacionados con la moda, el arte y la decoración.</label>
                        </div>
                        {/* pregunta21*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchHuma" onClick={this.handleSwitchHuma} class="custom-control-input" id="customCheck21"/>
                            <label class="custom-control-label" for="customCheck21">21- Conducir programas de radio.</label>
                        </div>
                        {/* pregunta22*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchHuma" onClick={this.handleSwitchHuma} class="custom-control-input" id="customCheck22"/>
                            <label class="custom-control-label" for="customCheck22">22- Dar clases en los distintos niveles educativos.</label>
                        </div>
                        {/* pregunta23*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchAgra" onClick={this.handleSwitchAgra} class="custom-control-input" id="customCheck23"/>
                            <label class="custom-control-label" for="customCheck23">23- Conservar, recuperar y aprovechar el ambiente natural.</label>
                        </div>
                        {/* pregunta24*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchIng" onClick={this.handleSwitchIng} class="custom-control-input" id="customCheck24"/>
                            <label class="custom-control-label" for="customCheck24">24- Reparar equipos de sonido, computadoras, televisores, etc.</label>
                        </div>
                        {/* pregunta25*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSegu" onClick={this.handleSwitchSegu} class="custom-control-input" id="customCheck25"/>
                            <label class="custom-control-label" for="customCheck25">25- Actividades que impliquen acción y despliegue de capacidades físicas.</label>
                        </div>
                        {/* pregunta26*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSalud" onClick={this.handleSwitchSalud} class="custom-control-input" id="customCheck26"/>
                            <label class="custom-control-label" for="customCheck26">26- Realizar acciones de prevención y promoción de la salud.</label>
                        </div>
                        {/* pregunta27*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchEco" onClick={this.handleSwitchEco} class="custom-control-input" id="customCheck27"/>
                            <label class="custom-control-label" for="customCheck27">27- Organizar sistemas de información para una mejor administración y gestión empresarial.</label>
                        </div>
                        {/* pregunta28*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchAgra" onClick={this.handleSwitchAgra} class="custom-control-input" id="customCheck28"/>
                            <label class="custom-control-label" for="customCheck28">28- Capacitar sobre las estrategias tecnológicas productivas mas adecuadas para la agricultura familiar y pueblos originarios.</label>
                        </div>
                        {/* pregunta29*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchArt" onClick={this.handleSwitchArt} class="custom-control-input" id="customCheck29"/>
                            <label class="custom-control-label" for="customCheck29">29- Asistir a conciertos y audiciones musicales.</label>
                        </div>
                        {/* pregunta30*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchHuma" onClick={this.handleSwitchHuma} class="custom-control-input" id="customCheck30"/>
                            <label class="custom-control-label" for="customCheck30">30- Estudiar el origen y evolución de las lenguas.</label>
                        </div>
                        {/* pregunta31*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchArt" onClick={this.handleSwitchArt} class="custom-control-input" id="customCheck31"/>
                            <label class="custom-control-label" for="customCheck31">31- Imaginar formas y colores y combinarlos armónicamente.</label>
                        </div>
                        {/* pregunta32*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSegu" onClick={this.handleSwitchSegu} class="custom-control-input" id="customCheck32"/>
                            <label class="custom-control-label" for="customCheck32">32- Desarrollar tus actividades con estrategia, disciplina y organización.</label>
                        </div>
                        {/* pregunta33*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchAgra" onClick={this.handleSwitchAgra} class="custom-control-input" id="customCheck33"/>
                            <label class="custom-control-label" for="customCheck33">33- Participar en el diseño y gestión proyectos de emprendimiento agroindustriales.</label>
                        </div>
                        {/* pregunta34*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSalud" onClick={this.handleSwitchSalud} class="custom-control-input" id="customCheck34"/>
                            <label class="custom-control-label" for="customCheck34">34- Trabajar en un hospital, clínica o puestos de salud.</label>
                        </div>
                        {/* pregunta35*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchEco" onClick={this.handleSwitchEco} class="custom-control-input" id="customCheck35"/>
                            <label class="custom-control-label" for="customCheck35">35- Escribir un artículo en la sección económica del diario.</label>
                        </div>
                        {/* pregunta36*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchIng" onClick={this.handleSwitchIng} class="custom-control-input" id="customCheck36"/>
                            <label class="custom-control-label" for="customCheck36">36- Diseñar y construir obras.</label>
                        </div>
                        {/* pregunta37*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchArt" onClick={this.handleSwitchArt} class="custom-control-input" id="customCheck37"/>
                            <label class="custom-control-label" for="customCheck37">37- Rediseñar y decorar espacios físicos.</label>
                        </div>
                        {/* pregunta38*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchIng" onClick={this.handleSwitchIng} class="custom-control-input" id="customCheck38"/>
                            <label class="custom-control-label" for="customCheck38">38- Elaborar y diseñar programas informáticos.</label>
                        </div>
                        {/* pregunta39*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchArt" onClick={this.handleSwitchArt} class="custom-control-input" id="customCheck39"/>
                            <label class="custom-control-label" for="customCheck39">39- Modelar objetos de cerámica, diseñar prendas de vestir, joyas, etc.</label>
                        </div>
                        {/* pregunta40*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchHuma" onClick={this.handleSwitchHuma} class="custom-control-input" id="customCheck40"/>
                            <label class="custom-control-label" for="customCheck40">40- Participar de un concurso literario</label>
                        </div>
                        {/* pregunta41*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchEco" onClick={this.handleSwitchEco} class="custom-control-input" id="customCheck41"/>
                            <label class="custom-control-label" for="customCheck41">41- Trabajar en un área de importación y exportación de productos.</label>
                        </div>
                        {/* pregunta42*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSegu" onClick={this.handleSwitchSegu} class="custom-control-input" id="customCheck42"/>
                            <label class="custom-control-label" for="customCheck42">42- Intervenir rápidamente en una situación de emergencia.</label>
                        </div>
                        {/* pregunta43*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchAgra" onClick={this.handleSwitchAgra} class="custom-control-input" id="customCheck43"/>
                            <label class="custom-control-label" for="customCheck43">43- Colaborar en el cuidado de la naturaleza y bosques nativos.</label>
                        </div>
                        {/* pregunta44*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSalud" onClick={this.handleSwitchSalud} class="custom-control-input" id="customCheck44"/>
                            <label class="custom-control-label" for="customCheck44">44- Realizar tratamiento a niños con problemas del lenguaje.</label>
                        </div>
                        {/* pregunta45*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchArt" onClick={this.handleSwitchArt} class="custom-control-input" id="customCheck45"/>
                            <label class="custom-control-label" for="customCheck45">45- Participar de una obra de teatro.</label>
                        </div>
                        {/* pregunta46*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSalud" onClick={this.handleSwitchSalud} class="custom-control-input" id="customCheck46"/>
                            <label class="custom-control-label" for="customCheck46">46- Atender a la salud de personas enfermas.</label>
                        </div>
                        {/* pregunta47*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchIng" onClick={this.handleSwitchIng} class="custom-control-input" id="customCheck47"/>
                            <label class="custom-control-label" for="customCheck47">47- Realizar actividades relacionadas con la construcción.</label>
                        </div>
                        {/* pregunta48*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchArt" onClick={this.handleSwitchArt} class="custom-control-input" id="customCheck48"/>
                            <label class="custom-control-label" for="customCheck48">48- Restaurar obras de arte, tomando decisiones propias sobre la forma, modo y técnica a aplicar.</label>
                        </div>
                        {/* pregunta49*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchEco" onClick={this.handleSwitchEco} class="custom-control-input" id="customCheck49"/>
                            <label class="custom-control-label" for="customCheck49">49- Realizar una investigación que favorezca una distribución equitativa de la riqueza.</label>
                        </div>
                        {/* pregunta50*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchAgra" onClick={this.handleSwitchAgra} class="custom-control-input" id="customCheck50"/>
                            <label class="custom-control-label" for="customCheck50">50- Elaborar tecnologías que favorezcan los recursos productivos de una región.</label>
                        </div>
                        {/* pregunta51*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchHuma" onClick={this.handleSwitchHuma} class="custom-control-input" id="customCheck51"/>
                            <label class="custom-control-label" for="customCheck51">51- Realizar estudios sobre hábitos, comportamientos, valores y creencias de los grupos humanos actuales y pasados.</label>
                        </div>
                        {/* pregunta52*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchEco" onClick={this.handleSwitchEco} class="custom-control-input" id="customCheck52"/>
                            <label class="custom-control-label" for="customCheck52">52- Controlar ingresos y egresos de fondos realizando el balance de las actividades.</label>
                        </div>
                        {/* pregunta53*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSegu" onClick={this.handleSwitchSegu} class="custom-control-input" id="customCheck53"/>
                            <label class="custom-control-label" for="customCheck53">53- Utilizar uniformes.</label>
                        </div>
                        {/* pregunta54*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSalud" onClick={this.handleSwitchSalud} class="custom-control-input" id="customCheck54"/>
                            <label class="custom-control-label" for="customCheck54">54- Trabajar en un laboratorio con microscopios y compuestos químicos.</label>
                        </div>
                        {/* pregunta55*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchHuma" onClick={this.handleSwitchHuma} class="custom-control-input" id="customCheck55"/>
                            <label class="custom-control-label" for="customCheck55">55- Explicar a tus compañeros los temas que ellos no entienden.</label>
                        </div>
                        {/* pregunta56*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchIng" onClick={this.handleSwitchIng} class="custom-control-input" id="customCheck56"/>
                            <label class="custom-control-label" for="customCheck56">56-  Participar de explotaciones mineras de yacimientos metalíferos y no metalíferos.</label>
                        </div>
                        {/* pregunta57*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchArt" onClick={this.handleSwitchArt} class="custom-control-input" id="customCheck57"/>
                            <label class="custom-control-label" for="customCheck57">57- Realizar actividades relacionadas con el dibujo y la pintura</label>
                        </div>
                        {/* pregunta58*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSalud" onClick={this.handleSwitchSalud} class="custom-control-input" id="customCheck58"/>
                            <label class="custom-control-label" for="customCheck58">58- Investigar en un laboratorio el origen de algunas enfermedades.</label>
                        </div>
                        {/* pregunta59*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchAgra" onClick={this.handleSwitchAgra} class="custom-control-input" id="customCheck59"/>
                            <label class="custom-control-label" for="customCheck59">59- Estudiar la flora y fauna de diferentes lugares.</label>
                        </div>
                        {/* pregunta60*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchAgra" onClick={this.handleSwitchAgra} class="custom-control-input" id="customCheck60"/>
                            <label class="custom-control-label" for="customCheck60">60- Utilizar maquinaria tecnológica para la elaboración de subproductos lácteos.</label>
                        </div>
                        {/* pregunta61*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchEco" onClick={this.handleSwitchEco} class="custom-control-input" id="customCheck61"/>
                            <label class="custom-control-label" for="customCheck61">61- Desempeñarte como gerente de comercialización en una empresa.</label>
                        </div>
                        {/* pregunta62*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSegu" onClick={this.handleSwitchSegu} class="custom-control-input" id="customCheck62"/>
                            <label class="custom-control-label" for="customCheck62">62- Realizar funciones de seguridad y vigilancia.</label>
                        </div>
                        {/* pregunta63*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchHuma" onClick={this.handleSwitchHuma} class="custom-control-input" id="customCheck63"/>
                            <label class="custom-control-label" for="customCheck63">63- Debatir y argumentar en cuestiones morales, antropológicas, filosóficas, desde un posicionamiento crítico y reflexivo.</label>
                        </div>
                        {/* pregunta64*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchIng" onClick={this.handleSwitchIng} class="custom-control-input" id="customCheck64"/>
                            <label class="custom-control-label" for="customCheck64">64- Supervisar el control de calidad de los productos alimenticios elaborados.</label>
                        </div>
                        {/* pregunta65*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchAgra" onClick={this.handleSwitchAgra} class="custom-control-input" id="customCheck65"/>
                            <label class="custom-control-label" for="customCheck65">65- Vivir en una zona agrícola-ganadera para desarrollar tus actividades como profesional.</label>
                        </div>
                        {/* pregunta66*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSegu" onClick={this.handleSwitchSegu} class="custom-control-input" id="customCheck66"/>
                            <label class="custom-control-label" for="customCheck66">66- Perseguir, detener y poner a disposición judicial a aquellos que cometen delitos.</label>
                        </div>
                        {/* pregunta67*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchArt" onClick={this.handleSwitchArt} class="custom-control-input" id="customCheck67"/>
                            <label class="custom-control-label" for="customCheck67">67- Actuar, bailar distintos tipos de danzas, cantar.</label>
                        </div>
                        {/* pregunta68*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchEco" onClick={this.handleSwitchEco} class="custom-control-input" id="customCheck68"/>
                            <label class="custom-control-label" for="customCheck68">68- Aconsejar a personas sobre planes de ahorro e inversiones.</label>
                        </div>
                        {/* pregunta69*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchIng" onClick={this.handleSwitchIng} class="custom-control-input" id="customCheck69"/>
                            <label class="custom-control-label" for="customCheck69">69- Aplicar conocimientos matemáticos al diseño de planos o maquetas.</label>
                        </div>
                        {/* pregunta70*/}
                        <div class="custom-control custom-checkbox" style={{paddingBottom:20}}>
                            <input type="checkbox" name="switchSalud" onClick={this.handleSwitchSalud} class="custom-control-input" id="customCheck70"/>
                            <label class="custom-control-label" for="customCheck70">70- Descubrir nuevas vacunas para las enfermedades actuales.</label>
                        </div>
                    </div>
                    <div>

                        {/* Humanidaes {this.state.contHuma}<br></br>
                        Economicas {this.state.contEco}<br></br>
                        Ciencias de Salud {this.state.contSalud}<br></br>
                        Seguridad Publica {this.state.contSegu}<br></br>
                        Ingenieria {this.state.contIng}<br></br>
                        Agrarias {this.state.contAgra}<br></br>
                        Ciencias Artisticas {this.state.contArt}<br></br> */}

                        {/* botonModal */}
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Evaluar tus respuestas
                        </button>

                        {/* cuerpoModal */}
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel"><b>Resultado de Intereses:</b></h4>
                                </div>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Los siguientes porcentajes reflejan tus intereses vocacionales  </h5>
                                    <div className="container">
                                                <img
                                                    src={chicos}
                                                    alt=""
                                                    width="500"
                                                    height="auto"
                                                    className="logoe rounded mx-auto d-block img-fluid"
                                                />
                                                </div>
                                </div>
                                <div class="modal-body">
                                
                                    A- Humanidades y Cs. Sociales: <b>{(this.state.contHuma)*10}%</b>  <Link to="/humanidades" target="_blank">Ver Más</Link><br></br>
                                    B- Ciencias Económicas y Administrativas: <b>{(this.state.contEco)*10}% </b> <Link to="/economicas" target="_blank">Ver Más</Link><br></br>
                                    C- Ciencias de la Salud: <b>{(this.state.contSalud)*10}%</b><br></br>
                                    D- Seguridad: <b>{(this.state.contSegu)*10}%</b><br></br>
                                    E- Ingeniería e informática: <b>{(this.state.contIng)*10}%</b>  <Link to="/ingenieria" target="_blank">Ver Más</Link><br></br>
                                    F- Ciencias Agrarias: <b>{(this.state.contAgra)*10}%</b> <Link to="/agrarias" target="_blank">Ver Más</Link><br></br>
                                    G- Artísticas: <b>{(this.state.contArt)*10}%</b><br></br>

                                </div>
                                <div >
                                    
                                
                                        {/* <button type="button" class="btn btn-success btn-lg" onClick={this.handleNewTest} data-dismiss="modal" style={{float:"right"}}>Volver</button> */}
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onClick={this.handleNewTest}>Volver a hacer el test</button>
                                              
                                </div>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                   
                </div>
        );
    }
}

export default Cuestionario;
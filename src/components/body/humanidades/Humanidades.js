import * as React from 'react';
import { Link } from "react-router-dom";
import YouTube from 'react-youtube';


import { MDBCardImage, MDBCardTitle, MDBCardText, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler,
    MDBMask, MDBRow, MDBCol, MDBBtn, MDBView, MDBContainer, MDBCard, MDBCardBody,
    MDBInput, MDBFormInline, MDBBadge, MDBIcon } from 'mdbreact';

import './humanidades.css'
import logohuma from './logohuma.png'
import logosiu from './logosiu.png'
import humafoot from './humafoot.png'

class Humanidades extends React.Component {

    constructor(props) {
        super(props);
    }   
    componentDidMount() {
        window.scrollTo(0, 0)
      } 
  render() {
   
    // variables YouTubeReact
    const opts = {
        height: '390',
        width: '640',
        playerVars: { // https://developers.google.com/youtube/player_parameters
          autoplay: 0,
          controls:0,
          showinfo:0
        }
      };
      
    return (
        <div>
            <div className="humaapppage">
                
                <MDBView>
                    <MDBMask className="gradient">
                    <div class="row align-items-center mt-5">

                        
                        <div className="col-md-6 col-10 white-text"> 
                            <h1 className="aligntexto">Facultad de Humanidades y Ciencias Sociales</h1>
                        </div>
                        <div className="col-md-6 d-none d-md-block mt-3">
                            <a href="http://www.fhycs.unju.edu.ar/index.php" target="_blank">
                                <img

                                    src={logohuma}
                                    alt=""
                                    width="250"
                                    className="logoh"
                                />
                            </a>
                        </div>  
                        

                    </div>
                    </MDBMask>
                    
                </MDBView>
                
            </div>

            

            <div>

                <MDBCard className="my-5 px-3 mx-auto"style={{ fontWeight: 300 }}>
                    <MDBCardBody >


                        
                                        <MDBRow>

                                            {/* 1 Lic Antropologia */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">

                                            <div className="embed-responsive embed-responsive-16by9">
                                                <YouTube
                                                    videoId="lU9RNP4nTGI"
                                                    opts={opts}
                                                    onReady={this._onReady}
                                                    />
                                            </div>

                                            <MDBCol size="12" className="p-4 text-center">
                                                <a href="http://www.fhycs.unju.edu.ar/ingreso/ifLicAntropologia.html" target="_blank">
                                                <h5>
                                                <p className="font-weight-light textenlace">Licenciatura en Antropología</p>
                                                </h5>
                                                </a>
                                            </MDBCol>    
                                            </MDBCol>

                                            {/* 2 Licenciatura en Educación para la Salud */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">

                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="g6MGicfs2vw"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                        />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fhycs.unju.edu.ar/ingreso/ifLicEduSalud.html" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licenciatura en Educación para la Salud</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 3 Lic en letras */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="LwJwxLAJmtc"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fhycs.unju.edu.ar/ingreso/ifLicAntropologia.html" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licenciatura en Letras</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 4 Prof y Lic en cs de la educacion */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="yo1djTB64cY"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fhycs.unju.edu.ar/ingreso/ifProfLicCiaEdu.html" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Profesorado y Licenciatura en Ciencias de la Educación</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 5 Lic com social */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="YM5WjQz7g2w"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fhycs.unju.edu.ar/ingreso/ifLicComSocial.html" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licenciatura en Comunicación Social</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 6 Lic trabajo social */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                    
                                                        videoId="CB-AkNrvCVA"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fhycs.unju.edu.ar/ingreso/ifLicTrabSocial.html" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licenciatura en Trabajo Social</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 7 Profesorado y Lic en historia */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="vv7HlAvDuoY"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fhycs.unju.edu.ar/ingreso/ifLicHistoria.html" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Profesorado y Licenciatura en Historia</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            
                                            {/* 8 Prof y lic en filosofia */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="g2IyV6Lr4j0"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fhycs.unju.edu.ar/ingreso/ifProfLicFilosofia.html" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Profesorado y Licenciatura en Filosofía</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 9 Lic en turismo */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="8N9GqCJImpY"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fhycs.unju.edu.ar/ingreso/ifTecLicTurismo.html" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licenciatura en Turismo</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            


                                            

                                            
                                        </MDBRow>

                            {/* facuFooter - mobile */}
                            <div className="text-center col-sm-12 d-block d-sm-none">
                            
                                <hr />
                                <a href="http://www.fhycs.unju.edu.ar/" target="_blank">
                                    <img
                                        src={humafoot}
                                        alt=""
                                        width="150px"
                                        className="img-fluid"
                                    />
                                </a>
                            </div>  
                            {/* siu */}
                            <MDBCol md="12" xl="12" className="text-center">
                                <hr />
                                <a href="http://www.alumnos.fhycs.unju.edu.ar/fhycs/" target="_blank">
                                    <img
                                        src={logosiu}
                                        alt=""
                                        className="img-fluid"
                                    />
                                </a>
                            </MDBCol>  



                    </MDBCardBody>
                </MDBCard>
            </div>
        

            <Link to="/">
                <MDBBtn>Volver</MDBBtn>
            </Link>
           
        </div>

    );
  }
}

export default Humanidades;
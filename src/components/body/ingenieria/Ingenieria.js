import * as React from 'react';
import { Link } from "react-router-dom";
import YouTube from 'react-youtube';

import { MDBCardImage, MDBCardTitle, MDBCardText, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler,
    MDBMask, MDBRow, MDBCol, MDBBtn, MDBView, MDBContainer, MDBCard, MDBCardBody,
    MDBInput, MDBFormInline, MDBBadge, MDBIcon } from 'mdbreact';

import './ingenieria.css'
import logoing from './logoing.png'
import logosiu from './logosiu.png'
import ingfoot from './ingfoot.png'

class Ingenieria extends React.Component {

    constructor(props) {
        super(props);

        
    }   
    componentDidMount() {
        window.scrollTo(0, 0)
      } 
   
  render() {
   
    // variables YouTubeReact
    const opts = {
        height: '390',
        width: '640',
        playerVars: { // https://developers.google.com/youtube/player_parameters
          autoplay: 0,
          controls:0,
          showinfo:0
        }
      };

    return (
        <div>
            <div className="miapppage">
                
                <MDBView>
                    <MDBMask className="gradient">
                        <div class="row align-items-center mt-5">

                            
                            <div className="col-md-6 col-10 white-text mt-5">
                                <h1 className="aligntexto">Facultad de Ingeniería</h1>
                            </div>
                            <div className="col-md-6 d-none d-md-block">
                                <a href="https://fi.unju.edu.ar/" target="_blank">
                                    <img
                                        src={logoing}
                                        alt=""
                                        width="500"
                                        className="logoi"
                                    />
                                </a>
                            </div>  
                            

                        </div>
                    </MDBMask>
                    
                </MDBView>
                
            </div>

            

            <div>

                <MDBCard className="my-5 px-3 mx-auto"style={{ fontWeight: 300 }}>
                    <MDBCardBody >


                        
                                        <MDBRow>

                                            {/* Ciclo comun articulado */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">

                                            <div className="embed-responsive embed-responsive-16by9">
                                                <YouTube
                                                
                                                    videoId="lvaVhNzYsoY"
                                                    opts={opts}
                                                    onReady={this._onReady}
                                                    />
                                            </div>

                                            <MDBCol size="12" className="p-4 text-center">
                                                <a href="https://fi.unju.edu.ar/" target="_blank">
                                                <h5>
                                                        <p className="font-weight-light textenlace">Ciclo Común Articulado con Tucumán FI</p>
                                                </h5>
                                                </a>
                                            </MDBCol>    
                                            </MDBCol>

                                            {/* Ingenieria Informatica */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">

                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                    
                                                        videoId="TVmnl4NX7dQ"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                        />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="https://fi.unju.edu.ar/perfil-y-alcance-ing-informatica" target="_blank">
                                                    <h5>
                                                        <p className="font-weight-light textenlace">Ingenieria Informática</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* Ingenieria Industrial */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="8zIcwlHSvK0"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="https://fi.unju.edu.ar/perfil-y-alcance-ing-industrial" target="_blank">
                                                    <h5>
                                                        <p className="font-weight-light textenlace">Ingenieria Industrial</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* Ingenieria Quimica */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="onK8tsFEwgY"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="https://fi.unju.edu.ar/perfil-y-alcance-ing-quimica" target="_blank">
                                                    <h5>
                                                        <p className="font-weight-light textenlace">Ingenieria Química</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* Ingenieria en Minas */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="4Ot3z84MCds"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="https://fi.unju.edu.ar/perfil-y-alcance-ing-de-minas" target="_blank">
                                                    <h5>
                                                        <p className="font-weight-light textenlace">Ingenieria en Minas</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* Lic Sistemas */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                    
                                                        videoId="fOhun_we_qw"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="https://fi.unju.edu.ar/perfil-y-alcance-lic-en-sistemas" target="_blank">
                                                    <h5>
                                                        <p className="font-weight-light textenlace">Licenciatura en Sistemas</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* Lic Alimentos */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="ilrKDU6VMWc"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="https://fi.unju.edu.ar/perfil-y-alcance-lic-en-tecn-de-los-alimentos" target="_blank">
                                                    <h5>
                                                        <p className="font-weight-light textenlace">Licenciatura en Tecnología de los Alimentos</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            
                                            {/* Lic Geologia */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                    
                                                        videoId="ekXJRCcstww"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="https://fi.unju.edu.ar/perfil-y-alcance-lic-en-cs-geologicas" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licenciatura en Cs Geológicas</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* Tecnicatura petroleo */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                    
                                                        videoId="A8dHeUo7K7o"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="https://fi.unju.edu.ar/perfil-y-alcance-tec-univ-en-cs-de-la-tierra-orientada-a-petroleo" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Tecnictura en Cs. de la Tierra con orientación en Petroleo</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* Tecnicatura en perforaciones */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                    
                                                        videoId="obtH2g-AhRA"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="https://fi.unju.edu.ar/perfil-y-alcance-tec-univ-en-perforaciones" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Tecnictura Universitaria en Perforaciones</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>


                                            

                                            
                                        </MDBRow>
                            {/* facuFooter - mobile */}
                            <div className="text-center col-sm-12 d-block d-sm-none">
                            
                                <hr />
                                <a href="https://fi.unju.edu.ar/" target="_blank">
                                    <img
                                        src={ingfoot}
                                        alt=""
                                        width="2000px"
                                        className="img-fluid"
                                    />
                                </a>
                            </div>  
                            {/* siu */}
                            <MDBCol md="12" xl="12" className="text-center">
                                <hr />
                                <a href="http://www.alumnos.fi.unju.edu.ar/ingenieria/" target="_blank">
                                    <img
                                        src={logosiu}
                                        alt=""
                                        className="img-fluid"
                                    />
                                </a>
                            </MDBCol>  



                    </MDBCardBody>
                </MDBCard>
            </div>
        

            <Link to="/">
                <MDBBtn>Volver</MDBBtn>
            </Link>
           
        </div>

    );
  }
}

export default Ingenieria;
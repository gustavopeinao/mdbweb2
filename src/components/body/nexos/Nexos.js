import * as React from 'react';
import { Link } from "react-router-dom";
import { MDBCardImage, MDBCardTitle, MDBCardText, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler,
    MDBMask, MDBRow, MDBCol, MDBBtn, MDBView, MDBContainer, MDBCard, MDBCardBody,
    MDBInput, MDBFormInline, MDBAvatar, MDBIcon } from 'mdbreact';

import './Nexos.css'
import fotoing from './fotoing.jpg'
import tutorias from './tutorias.jpg'
import cdocentes from './cdocentes.jpg'
import cepa from './cepa.jpg'
import ofertas from './ofertas.jpg'
import iconox from './iconox.png'


class Nexos extends React.Component {

  componentDidMount() {
    window.scrollTo(0, 0)
  } 
  render() {
    
    

    return (
        <div>
          <div class="container">
            <div class="row align-items-center">
              <div className="card col-md-12" style={{ fontWeight: 300 }}>

              
                <MDBCardBody>
                  <h2 className="h1-responsive font-weight-bold my-5 text-center">
                    Programa Nexos
                  </h2>
                  <MDBRow>
                    <MDBCol>
                      <p className="font-weight-light text-justify">
                      Ha sido pensado como una estrategia de integración entre los distintos niveles (medio y superior) y ámbitos del sistema educativo cuya meta es impulsar una política pública que promueva la construcción de un diagnóstico compartido y el diseño de un plan de trabajo común, sustentado en el subprograma UNIVERSIDAD – ESCUELA SECUNDARIA y EDUCACIÓN SUPERIOR UNIVERSITARIA.
                      En la última convocatoria la Secretaria de Extensión de la Universidad Nacional de Jujuy obtuvo la aprobación del proyecto enmarcado en el SUB-PROGRAMA DE ARTICULACIÓN UNIVERSIDAD-ESCUELAS SECUNDARIA. Por lo que ha iniciado una sucesión de acciones que como lo sugiere el programa permitan promover y mejorar la articulación interinstitucional entre el nivel secundario y la educación superior, asegurando la participación de los distintos actores del sistema educativo, las Jurisdicciones Provinciales y las Universidades.
                      Como resultado de las primeras acciones planificadas de forma consensuada y colaborativa desde el programa NEXOS, se ha logrado desarrollar producciones, talleres y espacios de trabajo dialectico, sustentados en el diagnóstico previo a fin de cubrir las necesidades de cada contexto.
                      </p>
                    </MDBCol>
                  </MDBRow>

                  <MDBCol md="12">
                  <h4 className="h3-responsive font-weight-bold my-5">
                    Lineas de Trabajo del Sub-Programa
                  </h4>

                  </MDBCol>

                  <MDBRow>
                  {/* Tutorias para el ingreso */}
                  <MDBCol md="6" className="md-0 mb-4">
                      <MDBCard
                        className="card-image"
                        style={{
                          backgroundImage: 'url(' + tutorias + ')'
                          
                        }}
                      >
                      <div className="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4 rounded">
                        <div>
                              <h6 className="blue-text">
                                <img
                                      src={iconox}
                                      width="30" 
                                      alt="logo"
                                      className="App-logo"
                                        
                                />
                                <strong> Lineas de Trabajo</strong>
                            </h6>
                          <h3 className="py-3 font-weight-bold">
                            <strong>TUTORIAS PARA EL INGRESO A LA UNJu</strong>
                          </h3>
                          <p className="pb-3">
                          La Coordinación de Extensión de la
                          Universidad Nacional de Jujuy brinda talleres informativos y de acompañamiento
                          pedagógico a estudiantes de escuelas secundarias ubicadas geográficamente en las
                          localidades en las que funcionan las sedes universitarias de la UNJu. (Humahuaca,
                          Tilcara, San Salvador de Jujuy, San Pedro).
                          </p>
                          {/* <MDBBtn color="pink" rounded size="md">
                            <MDBIcon far icon="clone" className="left" /> MDBView project
                          </MDBBtn> */}
                        </div>
                      </div>
                      </MDBCard>
                    </MDBCol>
                    {/* Capacitacion docente */}
                    <MDBCol md="6" className="md-0 mb-4">
                    <MDBCard
                        className="card-image"
                        style={{
                          backgroundImage: 'url(' + cdocentes + ')'
                        }}
                      >
                        <div className="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4 rounded">
                          <div>
                            <h6 className="blue-text">
                              <img
                                    src={iconox}
                                    width="30" 
                                    alt="logo"
                                    className="App-logo"
                                      
                              />
                              <strong> Lineas de Trabajo</strong>
                            </h6>
                            <h3 className="py-3 font-weight-bold">
                              <strong>CAPACITACION DOCENTE</strong>
                            </h3>
                            <p className="pb-3">
                            La Coordinación de Extensión junto a Docentes e
                            Investigadores de la UNJu desarrollan talleres de capacitación destinado a docentes de
                            escuelas de nivel medio con el propósito de desarrollar de manera conjunta diferentes
                            dispositivos para el acompañamiento de los alumnos del ultimo año del nivel
                            secundario que deseen continuar con sus estudios universitarios.
                            </p>
                            {/* <MDBBtn color="success" rounded size="md">
                              <MDBIcon far icon="clone" className="left" /> MDBView project
                            </MDBBtn> */}
                          </div>
                        </div>
                      </MDBCard>
                    </MDBCol>
                    {/* Desarrollo material audiovisual */}
                    <MDBCol md="6" className="md-0 mb-4">
                    <MDBCard
                        className="card-image"
                        style={{
                          backgroundImage: 'url(' + cepa + ')',
                          // width:650
                        }}
                        
                      >
                        <div className="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4 rounded">
                          <div>
                            <h6 className="blue-text">
                                <img
                                      src={iconox}
                                      width="30" 
                                      alt="logo"
                                      className="App-logo"
                                        
                                />
                                <strong> Lineas de Trabajo</strong>
                            </h6>
                            <h3 className="py-3 font-weight-bold">
                              <strong>DESARROLLO DE MATERIAL AUDIOVISUAL</strong>
                            </h3>
                            <p className="pb-3">
                            El CEPA de la Universidad Nacional de Jujuy
                            desarrolla material audiovisual con el objeto de promover a través de diferentes
                            formatos la oferta academica de las cuatro unidades académicas de la casa de altos
                            estudios.
                            </p>
                            {/* <MDBBtn color="success" rounded size="md">
                              <MDBIcon far icon="clone" className="left" /> MDBView project
                            </MDBBtn> */}
                          </div>
                        </div>
                      </MDBCard>
                    </MDBCol>
                    {/* Mapa de ofertas academicas */}
                    <MDBCol md="6" className="md-0 mb-4">
                    <MDBCard
                        className="card-image"
                        style={{
                          backgroundImage: 'url(' + ofertas + ')'
                        }}
                      >
                        <div className="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4 rounded">
                          <div>
                            <h6 className="blue-text">
                                <img
                                      src={iconox}
                                      width="30" 
                                      alt="logo"
                                      className="App-logo"
                                        
                                />
                                <strong> Lineas de Trabajo</strong>
                            </h6>
                            <h3 className="py-3 font-weight-bold">
                              <strong>MAPA DE OFERTAS ACADÉMICAS DE LA UNJu</strong>
                            </h3>
                            <p className="pb-3">
                            La
                            Coordinación de Extensión de la Universidad Nacional de Jujuy desarrolla herramientas
                            interactivas para la difusión de las ofertas académicas del nivel superior universitario.
                            </p>
                            {/* <MDBBtn color="success" rounded size="md">
                              <MDBIcon far icon="clone" className="left" /> MDBView project
                            </MDBBtn> */}
                          </div>
                        </div>
                      </MDBCard>
                    </MDBCol>




                  </MDBRow>




                          
                </MDBCardBody>
              
              
              
              </div>  
              <Link to="/">
                  <MDBBtn>Volver</MDBBtn>
              </Link>
            </div>
          </div>
        </div>
    );
  }
}

export default Nexos;
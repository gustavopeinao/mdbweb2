import * as React from 'react';
import { Link } from "react-router-dom";
import YouTube from 'react-youtube';

import { MDBCardImage, MDBCardTitle, MDBCardText, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler,
    MDBMask, MDBRow, MDBCol, MDBBtn, MDBView, MDBContainer, MDBCard, MDBCardBody,
    MDBInput, MDBFormInline, MDBBadge, MDBIcon } from 'mdbreact';

import './economicas.css'
import logoeco from './logoeco.png'
import logosiu from './logosiu.png'
import ecofoot from './ecofoot.png'


class Economicas extends React.Component {

    constructor(props) {
        super(props);

        
    }  
    componentDidMount() {
        window.scrollTo(0, 0)
      } 
   
  render() {
   
    // variables YouTubeReact
    const opts = {
        height: '390',
        width: '640',
        playerVars: { // https://developers.google.com/youtube/player_parameters
          autoplay: 0,
          controls:0,
          showinfo:0
        }
      };

    return (
        <div>
            <div className="ecoapppage">
                
                <MDBView>
                    <MDBMask className="gradient">
                    
                        <div class="row align-items-center mt-5">
                            <div className="col-md-6 col-10 white-text"> 
                                    <h1 className="aligntexto">Facultad de Ciencias Económicas</h1>     
                            </div>

                            <div className="col-md-6 d-none d-md-block mt-3">
                                <a href="http://www.fce.unju.edu.ar/" target="_blank">
                                    <img
                                        src={logoeco}
                                        alt=""
                                        width="200"
                                        height="200"
                                        className="logoe"
                                    />
                                </a>
                            </div>  
                        </div>

                    </MDBMask>   
                </MDBView>
                
            </div>

            

            <div>

                <MDBCard className="my-5 px-3 mx-auto"style={{ fontWeight: 300 }}>
                    <MDBCardBody >


                        
                                        <MDBRow>

                                            {/* Contador Publico */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">

                                            <div className="embed-responsive embed-responsive-16by9">
                                                <YouTube
                                                    videoId="u9phisBP2o0"
                                                    opts={opts}
                                                    onReady={this._onReady}
                                                    />
                                            </div>

                                            <MDBCol size="12" className="p-4 text-center">
                                                <a href="http://www.unju.edu.ar/Carreras/Economicas/contador_publico.html" target="_blank">
                                                <h5>
                                                <p className="font-weight-light textenlace">Contador Público</p>
                                                </h5>
                                                </a>
                                            </MDBCol>    
                                            </MDBCol>

                                            {/* Ingenieria Lic adm */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">

                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="z6PhJJ1hlrc"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                        />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.unju.edu.ar/Carreras/Economicas/licenciatura_administracion.html" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licenciatura en Administración de Empresas</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* Economia politica */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                    
                                                        videoId="1op6TxomyTI"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.unju.edu.ar/Carreras/Economicas" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Economía Politica</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

               
                                            
                                        </MDBRow>
                            {/* facuFooter - mobile */}
                            <div className="text-center col-sm-12 d-block d-sm-none">
                            
                                <hr />
                                <a href="http://www.fce.unju.edu.ar/" target="_blank">
                                    <img
                                        src={ecofoot}
                                        alt=""
                                        width="250px"
                                        className="img-fluid"
                                    />
                                </a>
                            </div>  
                            {/* siu */}
                            <MDBCol md="12" xl="12" className="text-center">
                                <hr />
                                <a href="http://www.alumnos.unju.edu.ar/fce/" target="_blank">
                                    <img
                                        src={logosiu}
                                        alt=""
                                        className="img-fluid"
                                    />
                                </a>
                            </MDBCol>  



                    </MDBCardBody>
                </MDBCard>
            </div>
        

            <Link to="/">
                <MDBBtn>Volver</MDBBtn>
            </Link>
           
        </div>

    );
  }
}

export default Economicas;
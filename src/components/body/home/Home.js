import * as React from 'react';
import { Link, BrowserRouter as Router } from "react-router-dom";
import {  MDBRow, MDBCol, MDBCard, MDBCardBody, MDBMask, MDBIcon, MDBView, MDBBtn, MDBContainer, MDBAvatar} from "mdbreact";
import './home.css'; 
import logoPcpal2 from './logoPcpal2.png';


class Home extends React.Component {
  state = {
    collapsed: false
  };

  componentDidMount() {
    window.scrollTo(0, 0)
  } 
  
  handleTogglerClick = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };
 
    render() {
      const navStyle = { marginTop: "4rem" };
      const overlay = (
        <div
          id="sidenav-overlay"
          style={{ backgroundColor: "transparent" }}
          onClick={this.handleTogglerClick}
        />
      );
      return (
        
          <div>
              {/* Portada */}
              <div className="apppage"> 
                <MDBView>
                  <MDBMask className="d-flex justify-content-center align-items-center gradient">
                    <MDBContainer>
                      <MDBRow>
                        <div className="white-text text-center text-md-left col-md-6 mt-xl-5 mb-5">
                          <h1 className="h1-responsive font-weight-bold mt-sm-5">Programa Nexos{" "}</h1>
                          <hr className="hr-light" />
                          <h6 className="mb-4">
                          Ha sido pensado como una estrategia de integración entre los distintos niveles (medio y superior) y ámbitos del sistema educativo cuya meta es impulsar una política pública que promueva la construcción de un diagnóstico compartido y el diseño de un plan de trabajo común, sustentado en el subprograma UNIVERSIDAD – ESCUELA SECUNDARIA y EDUCACIÓN SUPERIOR UNIVERSITARIA.</h6>     
                          <Link to="/nexos">
                            <MDBBtn outline color="white">Leer Más</MDBBtn>
                          </Link>
                        </div>
                        
                        
                        <div className="d-none d-sm-none d-md-block">
                          <img 
                            className="sombra img-fluid"
                            alt="aligment"
                            src={logoPcpal2}
                            width="350"
                            
                          />
                        </div>
                        {/* </MDBCol>  */}
                        
                      </MDBRow>
                    </MDBContainer>
                  </MDBMask>
                </MDBView>
              </div>
              {/* End Portada */}
              
              {/* Carreras */}
              <div>
              <MDBCard className="my-5 px-5 pb-5 text-center">
                <MDBCardBody>
                  <MDBRow>
                   


                        {/* INGENIERIA */}
                        <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
                          <MDBView className="overlay rounded z-depth-1" waves>
                            <img
                              src="http://us.cdn.salta.eltribuno.info.s3.amazonaws.com/032017/1488838166991.jpg"
                              alt=""
                              className="img-fluid"
                            />
                            <a>
                              <MDBMask overlay="white-slight" />
                            </a>
                          </MDBView>
                          
                          <MDBCardBody className="pb-0 text-center">
                            <h4 className="font-weight-bold my-3">Facultad de Ingeniería</h4>
                            {/* <p className="grey-text">
                              Temporibus autem quibusdam et aut officiis debitis aut rerum
                              necessitatibus saepe eveniet ut et voluptates repudiandae.
                            </p> */}
                            <Link to="/ingenieria">
                                <MDBBtn>Ver Carreras</MDBBtn>
                            </Link>
                          </MDBCardBody>
                        </MDBCol>

                        {/* ECONOMICAS */}
                        <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
                          <MDBView className="overlay rounded z-depth-1" waves>
                            <img
                              src="http://noticias.unju.edu.ar/noticias/serv/notis/74fotoecon.JPG"
                              alt=""
                              className="img-fluid"
                            />
                            <a href="">
                              <MDBMask overlay="white-slight" />
                            </a>
                          </MDBView>
                          <MDBCardBody className="pb-0 text-center">
                            <h4 className="font-weight-bold my-3">Facultad de Ciencias Economicas</h4>
                            {/* <p className="grey-text">
                              Temporibus autem quibusdam et aut officiis debitis aut rerum
                              necessitatibus saepe eveniet ut et voluptates repudiandae.
                            </p> */}
                            {/* <MDBBtn onClick={this.alPresionarBotonEco}>Ver Carreras</MDBBtn> */}
                            {/* <MDBBtn>
                              <Link to="/economicas">Ver Carreras</Link>
                            </MDBBtn> */}
                            <Link to="/economicas">
                                <MDBBtn>Ver Carreras</MDBBtn>
                            </Link>
                          </MDBCardBody>
                        </MDBCol>

                        {/* HUMANIDADES */}
                        <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
                          <MDBView className="overlay rounded z-depth-1" waves>
                            <img
                              src="http://www.jujuyaldia.com.ar/wp-content/uploads/2011/11/humanidades-patio-1.jpg"
                              alt=""
                              className="img-fluid"
                            />
                            <a href="">
                              <MDBMask overlay="white-slight" />
                            </a>
                          </MDBView>
                          <MDBCardBody className="pb-0 text-center">
                            <h4 className="font-weight-bold my-3">Facultad de Humanidades y Ciencias Sociales</h4>
                            {/* <p className="grey-text">
                              Temporibus autem quibusdam et aut officiis debitis aut rerum
                              necessitatibus saepe eveniet ut et voluptates repudiandae.
                            </p> */}
                            {/* <MDBBtn onClick={this.alPresionarBotonHum}>Ver Carreras</MDBBtn> */}
                            {/* <MDBBtn>
                              <Link to="/humanidades">Ver Carreras</Link>
                            </MDBBtn> */}
                            <Link to="/humanidades">
                                <MDBBtn>Ver Carreras</MDBBtn>
                            </Link>
                          </MDBCardBody>
                        </MDBCol>

                        {/* AGRARIAS */}
                        <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
                          <MDBView className="overlay rounded z-depth-1" waves>
                            <img
                              src="http://noticias.unju.edu.ar/noticias/serv/notis/100_FotoAgrarias.JPG"
                              alt=""
                              className="img-fluid"
                            />
                            <a href="">
                              <MDBMask overlay="white-slight" />
                            </a>
                          </MDBView>
                          <MDBCardBody className="pb-0 text-center">
                            <h4 className="font-weight-bold my-3">Facultad de Ciencias Agrarias</h4>
                            {/* <p className="grey-text">
                              Temporibus autem quibusdam et aut officiis debitis aut rerum
                              necessitatibus saepe eveniet ut et voluptates repudiandae.
                            </p> */}
                            {/* <MDBBtn onClick={this.alPresionarBotonAgr}>Ver Carreras</MDBBtn> */}
                            {/* <MDBBtn>
                              <Link to="/agrarias">Ver Carreras</Link>
                            </MDBBtn> */}
                            <Link to="/agrarias">
                                <MDBBtn>Ver Carreras</MDBBtn>
                            </Link>
                          </MDBCardBody>
                        </MDBCol>




                    
                  </MDBRow>                
                </MDBCardBody>
              </MDBCard>    
              </div>
              {/* End Carreras */}
          </div>
          

               


            
        )
    }
}

export default Home;
import * as React from 'react';
import { Link } from "react-router-dom";
import YouTube from 'react-youtube';

import { MDBCardImage, MDBCardTitle, MDBCardText, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler,
    MDBMask, MDBRow, MDBCol, MDBBtn, MDBView, MDBContainer, MDBCard, MDBCardBody,
    MDBInput, MDBFormInline, MDBBadge, MDBIcon } from 'mdbreact';

import './agrarias.css'
import logoagra from './logoagra.png'
import logosiu from './logosiu.png'
import agrafoot from './agrafoot.png'


class Agrarias extends React.Component {

    constructor(props) {
        super(props);

        
    }   
    componentDidMount() {
        window.scrollTo(0, 0)
      } 
   
  render() {
   
    // variables YouTubeReact
    const opts = {
        height: '390',
        width: '640',
        playerVars: { // https://developers.google.com/youtube/player_parameters
          autoplay: 0,
          controls:0,
          showinfo:0
        }
      };

    return (
        <div>
            <div className="agraapppage">
                
                <MDBView>
                    <MDBMask className="gradient">
                    <div class="row align-items-center mt-5">

                        
                        <div className="col-md-6 col-10 white-text mt-3"> 
                                    <h1 className="aligntexto">Facultad de Ciencias Agrarias</h1>     
                            </div>
                        <div className="col-md-6 d-none d-md-block mt-3">
                            <a href="http://www.fca.unju.edu.ar/" target="_blank">
                                <img
                                    src={logoagra}
                                    alt=""
                                    width="300"
                                    className="logoa"
                                />
                            </a>
                        </div>  
                        

                    </div>
                    </MDBMask>
                    
                </MDBView>
                
            </div>

            

            <div>

                <MDBCard className="my-5 px-3 mx-auto"style={{ fontWeight: 300 }}>
                    <MDBCardBody >


                        
                                        <MDBRow>

                                            {/* 1 Ing agronomica */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">

                                            <div className="embed-responsive embed-responsive-16by9">
                                                <YouTube
                                                    videoId="25PUPP1-Ba8"
                                                    opts={opts}
                                                    onReady={this._onReady}
                                                    />
                                            </div>

                                            <MDBCol size="12" className="p-4 text-center">
                                                <a href="http://www.fca.unju.edu.ar/academica/carreras-grado/ingenieria-agronomia/" target="_blank">
                                                <h5>
                                                <p className="font-weight-light textenlace">Ingeniería Agronómica</p>
                                                </h5>
                                                </a>
                                            </MDBCol>    
                                            </MDBCol>

                                            {/* 2 Lic bromatologia */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">

                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="7JGO_1S056c"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                        />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fca.unju.edu.ar/academica/carreras-grado/licenciatura-bromatologia/" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licenciatura en Bromatología</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 3 Lic en biologia */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                    
                                                        videoId="FyMliH02Kw0"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fca.unju.edu.ar/academica/carreras-grado/licenciatura-biologia/" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licenciatura en Ciencias Biologicas</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 4 Lic en des rural */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="Z1g462blu8k"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fca.unju.edu.ar/academica/carreras-grado/licenciatura-desarrollo-rural/" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licencia en Desarrollo Rural</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 5 Lic en gestion ambiental */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="2Rp9gQiSiUc"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fca.unju.edu.ar/academica/carreras-grado/licenciatura-gestion-ambiental/" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Licenciatura en Gestión Ambiental</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 6 Tec unv forestal */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="jUrNg2s-lM0"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fca.unju.edu.ar/academica/carreras-pregrado/tec-forestal/" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Tecnicatura Universitaria Forestal</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            {/* 7 Tec univ en prod lechera */}
                                            <MDBCol md="12" lg="4" className="mb-lg-0 mb-5">
                                                <div className="embed-responsive embed-responsive-16by9">
                                                    <YouTube
                                                        videoId="PAcdtXdfIYM"
                                                        opts={opts}
                                                        onReady={this._onReady}
                                                    />
                                                </div>
                                                
                                                <MDBCol size="12" className="p-4 text-center">
                                                    <a href="http://www.fca.unju.edu.ar/academica/carreras-pregrado/tec-produccion-lechera/" target="_blank">
                                                    <h5>
                                                    <p className="font-weight-light textenlace">Tecnicatura Universitaria en Producción Lechera</p>
                                                    </h5>
                                                    </a>
                                                </MDBCol>    
                                            </MDBCol>

                                            
                                            


                                            

                                            
                                        </MDBRow>
                            {/* facuFooter - mobile */}
                            <div className="text-center col-sm-12 d-block d-sm-none">
                            
                                <hr />
                                <a href="http://www.fca.unju.edu.ar/" target="_blank">
                                    <img
                                        src={agrafoot}
                                        alt=""
                                        width="150px"
                                        className="img-fluid"
                                    />
                                </a>
                            </div>  
                            {/* siu */}
                            <MDBCol md="12" xl="12" className="text-center">
                                <hr />
                                <a href="http://www.alumnos.unju.edu.ar/fce/" target="_blank">
                                    <img
                                        src={logosiu}
                                        alt=""
                                        className="img-fluid"
                                    />
                                </a>
                            </MDBCol>  



                    </MDBCardBody>
                </MDBCard>
            </div>
        

            <Link to="/">
                <MDBBtn>Volver</MDBBtn>
            </Link>
           
        </div>

    );
  }
}

export default Agrarias;
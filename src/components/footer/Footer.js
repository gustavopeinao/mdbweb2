import * as React from 'react';
// import '../../App.css';
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";
import logoFooter from './logoFooter.png';


class Footer extends React.Component {

    render() {
        return (
           <MDBFooter color="default-color-dark" className="font-small pt-4 mt-4">
            <MDBContainer fluid className="text-center text-md-left">
                <MDBRow>
                    <MDBCol md="6" style={{padding:0}}>
                        <div style={{padding:"20px"}}>
                            <h3 className="title">Programa Nexos</h3>
                            <a href="http://www.seu.unju.edu.ar/" target="_blank">
                                <h6>
                                    Secretaría de Extensión Universitaria
                                </h6>
                            </a>
                        </div>
                    </MDBCol>
                    <MDBCol md="6" style={{padding:0}}>
                        <MDBContainer>
                        <img
                            src={logoFooter}
                            width="500"
                            alt="logo"
                            className="img-fluid"
                            
                        />
                        </MDBContainer>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
            <div className="footer-copyright text-center py-3" style={{marginTop: '20px',}}>
                <MDBContainer fluid>
                &copy; {new Date().getFullYear()} Copyright: <a href=""> ABM - Seguridad Informática </a>
                </MDBContainer>
            </div>
    </MDBFooter>





                )
            }
        }
        
export default Footer;
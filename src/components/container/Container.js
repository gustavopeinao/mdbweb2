import * as React from 'react';
import { BrowserRouter, HashRouter, Switch, Route } from "react-router-dom";


import Navbar from '../navbar/Navbar';
import Footer from '../footer/Footer';
import Home from '../body/home/Home';
import Nexos from '../body/nexos/Nexos';
import Ingenieria from '../body/ingenieria/Ingenieria';
import Economicas from '../body/economicas/Economicas';
import Humanidades from '../body/humanidades/Humanidades';
import Agrarias from '../body/agrarias/Agrarias';
import TutoriasPage from '../../pages/TutoriasPage';
import SedesPage from '../../pages/SedesPage';
import CapacitacionPage from '../../pages/CapacitacionPage';
import Explorar from '../explorar/Explorar';
import Cuestionario from '../explorar/Cuestionario';



class Container extends React.Component {

   
  render() {
    return (
       <div> 
           <div>
                <Navbar />
            </div>
            <div style={{paddingTop: '30px'}}>
                
                    <Switch>
                        <Route path="/" component={Home} exact />
                        <Route path="/nexos" component={Nexos} exact />
                        <Route path="/ingenieria" component={Ingenieria} exact />
                        <Route path="/economicas" component={Economicas} exact />
                        <Route path="/humanidades" component={Humanidades} exact />
                        <Route path="/agrarias" component={Agrarias} exact />
                        <Route path="/tutorias" component={TutoriasPage} exact />
                        <Route path="/sedes" component={SedesPage} exact />
                        <Route path="/capacitacion" component={CapacitacionPage} exact />
                        <Route path="/explorar" component={Explorar} exact />
                        <Route path="/cuestionario" component={Cuestionario} exact />
                    </Switch>
                
            </div>
            <div>
                <Footer />  
            </div>
       </div>
    );
  }
}

export default Container;